# This file is part of pybib2html, a translator of BibTeX to HTML.
# https://gitlab.com/sosy-lab/software/pybib2html
#
# SPDX-FileCopyrightText: 2021 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

from typing import Any, Sequence
from bibtexparser.bibdatabase import BibDatabase

class BibTexWriter:
    contents: Any
    indent: str
    align_values: bool
    entry_separator: str
    order_entries_by: Any
    display_order: Sequence[str]
    comma_first: bool
    add_trailing_comma: bool
    common_strings: Any
    def __init__(self, write_common_strings: bool = ...) -> None: ...
    def write(self, bib_database: BibDatabase): ...
